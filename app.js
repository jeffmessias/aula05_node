const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
//body parser
app.use(express.urlencoded({
    extended: false
}));

app.get('/', (req, res) => {
    console.log("CHAMADO VIA GET!");
    res.send("Method GET - Route: /");
});

app.get('/usuario', (req, res) => {
    console.log("CHAMADO VIA GET - consulta usuário");
    res.send("Method GET - Route: /usuario");
});

app.get('/cliente', (req, res) => {
    console.log("CHAMADO VIA GET - consulta cliente");
    console.log(req.query);
    let nome = req.query.fname;
    let sobrenome = req.query.lname;
    //res.send("Method GET - Route: /cliente");
    res.send("Nome: " + nome + " " + sobrenome);
});

app.post('/cliente', (req, res) => {
    console.log("Method POST - Route: /cliente");
    res.send("Método post utilizado");
});

app.delete('/cliente', (req, res) => {
    console.log("Method DELETE - Route: /cliente");
    res.send("Método delete utilizado");
});

app.listen(8000, function () {
    console.log("projeto iniciado na porta 8000");
});